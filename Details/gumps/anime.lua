local _detalhes = 		_G._detalhes

--code from blizzard AlertFrames

function _detalhes:PlayGlow (frame)
	frame:Show()
	
	frame.glow:Show()
	frame.glow.animIn:Play()
	frame.shine:Show()
	frame.shine.animIn:Play()
	
	--PlaySound ("LFG_Rewards", "master")
end

--> WatchFrame copy, got removed on WoD
local function DetailsTutorialAlertFrame_OnFinishSlideIn (frame)
	frame.ScrollChild.Shine:Show();
	frame.ScrollChild.IconShine:Show();
	frame.ScrollChild.Shine.Flash:Play();
	frame.ScrollChild.IconShine.Flash:Play();
end

local function DetailsTutorialAlertFrame_OnUpdate (frame, timestep)
	local animData = frame.animData;
	local height = animData.height;
	local scrollStart = animData.scrollStart;
	local scrollEnd = animData.scrollEnd;
	local endTime = animData.slideInTime + (animData.endDelay or 0);

	if (frame.startDelay) then
		frame.startDelay = frame.startDelay - timestep;
		if (frame.startDelay <= 0) then
			frame.startDelay = nil;
		else
			return;
		end
	end

	if (frame.isFirst) then
		height = height + 10;
		scrollEnd = scrollEnd - 10;
	end

	frame.totalTime = frame.totalTime+timestep;
	if (frame.totalTime > endTime) then
		frame.totalTime = endTime;
	end

	local scrollPos = scrollEnd;
	if (animData.slideInTime and animData.slideInTime > 0) then
		height = height*(frame.totalTime/animData.slideInTime);
		scrollPos = scrollStart + (scrollEnd-scrollStart)*(frame.totalTime/animData.slideInTime);
	end
	if ( animData.reverse ) then
		height = max(animData.height - height, 1);
	end
	frame:SetHeight(height);
	frame:UpdateScrollChildRect();
	frame:SetVerticalScroll(floor(scrollPos+0.5));

	if (frame.totalTime >= endTime) then
		frame:SetScript("OnUpdate", nil);
		if ( animData.onFinishFunc ) then
			animData.onFinishFunc(frame);
		end
	end
end

function DetailsTutorialAlertFrame_SlideInFrame (frame, animType)
	frame.totalTime = 0;
	frame.animData = { height = 72, scrollStart = 65, scrollEnd = -9, slideInTime = 0.4, onFinishFunc = DetailsTutorialAlertFrame_OnFinishSlideIn };
	frame.slideInTime = frame.animData.slideInTime;
	frame:SetHeight(1);
	if ( frame.animData.reverse ) then
		frame:SetHeight(frame.animData["height"]);
	else
		frame:SetHeight(1);
	end
	frame.startDelay = frame.animData.startDelay;
	frame:SetScript("OnUpdate", DetailsTutorialAlertFrame_OnUpdate);
end

function _detalhes.PlayBestDamageOnGuild (damage)

	damage = damage or 100000000

	--> create the main frame
	local DetailsNewDamageRecord = CreateFrame ("frame", "DetailsNewDamageRecordAnimationFrame", UIParent)
	DetailsNewDamageRecord:SetPoint ("CENTER", UIParent, "CENTER", 0, -200)
	DetailsNewDamageRecord:SetSize (300, 300)

	--> single animation group
	local MainAnimationGroup = DetailsNewDamageRecord:CreateAnimationGroup ("DetailsNewDamageRecordAnimationGroup")
	MainAnimationGroup:SetLooping ("NONE")

	--> widgets:

	----------------------------------------------

	local BaseTexture  = DetailsNewDamageRecord:CreateTexture ("BaseTextureTexture", "ARTWORK")
	BaseTexture:SetTexture ([[Interface\ACHIEVEMENTFRAME\UI-Achievement-Alert-Background-Mini]])
	BaseTexture:SetDrawLayer ("ARTWORK", -5)
	BaseTexture:SetPoint ("center", DetailsNewDamageRecord, "center", 0, 0)
	BaseTexture:SetSize (256, 64)
	BaseTexture:SetVertexColor (0.99999779462814, 0.99999779462814, 0.99999779462814, 0.99999779462814)
	
	--> animations for BaseTexture

	BaseTexture.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	BaseTexture.fadein:SetOrder (1)
	BaseTexture.fadein:SetDuration (0.14869952201843)
    BaseTexture.fadein:SetStartDelay (0)
	BaseTexture.fadein:SetEndDelay (0)
	BaseTexture.fadein:SetChange(1)
	BaseTexture.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	BaseTexture.fadeout:SetOrder (2)
	BaseTexture.fadeout:SetDuration (1)
    BaseTexture.fadeout:SetStartDelay (5)
	BaseTexture.fadeout:SetEndDelay (0)
	BaseTexture.fadeout:SetChange(-1)

	----------------------------------------------

	local BigFlash  = DetailsNewDamageRecord:CreateTexture ("BigFlashTexture", "OVERLAY")
	BigFlash:SetTexture ([[Interface\ACHIEVEMENTFRAME\UI-Achievement-Alert-Glow]])
	BigFlash:SetDrawLayer ("OVERLAY", 0)
	BigFlash:SetPoint ("center", DetailsNewDamageRecord, "center", -2, 2)
	BigFlash:SetSize (314, 100)
	BigFlash:SetDesaturated (false)
	BigFlash:SetTexCoord (0.0010000000149012, 0.77400001525879, 0.0010000000149012, 0.65800003051758)
	if (0 ~= 0) then
	    BigFlash:SetRotation (0)
	end
	BigFlash:SetVertexColor (0.96470373868942, 0.98823314905167, 0.99999779462814, 0.99999779462814)
	BigFlash:SetAlpha (1)
	BigFlash:SetBlendMode ("ADD")

	--> animations for BigFlash
    BigFlash.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	BigFlash.fadein:SetOrder (1)
	BigFlash.fadein:SetDuration (0.11600000411272)
    BigFlash.fadein:SetStartDelay (0)
	BigFlash.fadein:SetEndDelay (0)
	BigFlash.fadein:SetChange(1)
	BigFlash.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	BigFlash.fadeout:SetOrder (2)
	BigFlash.fadeout:SetDuration (0.31600001454353)
    BigFlash.fadeout:SetStartDelay (0)
	BigFlash.fadeout:SetEndDelay (0)
	BigFlash.fadeout:SetChange(-1)

	----------------------------------------------

	local FlashSwipe  = DetailsNewDamageRecord:CreateTexture ("FlashSwipeTexture", "OVERLAY")
	FlashSwipe:SetTexture ([[Interface\ACHIEVEMENTFRAME\UI-Achievement-Alert-Glow]])
	FlashSwipe:SetDrawLayer ("OVERLAY", 7)
	FlashSwipe:SetPoint ("center", DetailsNewDamageRecord, "center", -99, 0)
	FlashSwipe:SetSize (100, 57)
	FlashSwipe:SetDesaturated (false)
	FlashSwipe:SetTexCoord (0.78199996948242, 0.91900001525879, 0.0010000000149012, 0.2760000038147)
	if (0 ~= 0) then
	    FlashSwipe:SetRotation (0)
	end
	FlashSwipe:SetVertexColor (0.86666476726532, 0.54117530584335, 0, 0.99999779462814)
	FlashSwipe:SetAlpha (1)
	FlashSwipe:SetBlendMode ("ADD")

	--> animations for FlashSwipe

    FlashSwipe.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	FlashSwipe.fadein:SetOrder (1)
	FlashSwipe.fadein:SetDuration (0.31600001454353)
    FlashSwipe.fadein:SetStartDelay (0.20000000298023)
	FlashSwipe.fadein:SetEndDelay (0)
	FlashSwipe.fadein:SetChange(0.5)
    FlashSwipe.translation = MainAnimationGroup:CreateAnimation ("TRANSLATION")
	FlashSwipe.translation:SetTarget (FlashSwipe)
	FlashSwipe.translation:SetOrder (1)
	FlashSwipe.translation:SetDuration (0.81599998474121)
	FlashSwipe.translation:SetStartDelay (0.20000000298023)
	FlashSwipe.translation:SetEndDelay (0)
	FlashSwipe.translation:SetOffset (200, 0)
	FlashSwipe.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	FlashSwipe.fadeout:SetOrder (1)
	FlashSwipe.fadeout:SetDuration (0.31600001454353)
    FlashSwipe.fadeout:SetStartDelay (0.69999998807907)
	FlashSwipe.fadeout:SetEndDelay (0)
	FlashSwipe.fadeout:SetChange(-0.5)

	----------------------------------------------

	local Portrait  = DetailsNewDamageRecord:CreateTexture ("PortraitTexture", "OVERLAY")
	Portrait:SetTexture ([[Interface\ARCHEOLOGY\ARCH-FLAREEFFECT]])
	Portrait:SetDrawLayer ("OVERLAY", -5)
	Portrait:SetPoint ("center", DetailsNewDamageRecord, "center", 3, 0)
	Portrait:SetSize (246, 44)
	Portrait:SetDesaturated (false)
	Portrait:SetTexCoord (0.051753740310669, 0.81701484680176, 0.086334381103516, 0.25102617263794)
	if (0 ~= 0) then
	    Portrait:SetRotation (0)
	end
	Portrait:SetVertexColor (0.99999779462814, 0.99999779462814, 0.99999779462814, 0.99999779462814)
	Portrait:SetAlpha (1)
	Portrait:SetBlendMode ("BLEND")

	--> animations for Portrait

    Portrait.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	Portrait.fadein:SetOrder (1)
	Portrait.fadein:SetDuration (0.41600000858307)
    Portrait.fadein:SetStartDelay (0)
	Portrait.fadein:SetEndDelay (0)
	Portrait.fadein:SetChange(0.5)
	Portrait.scale = MainAnimationGroup:CreateAnimation ("SCALE")
	Portrait.scale:SetTarget (Portrait)
	Portrait.scale:SetOrder (1)
	Portrait.scale:SetDuration (0.21600000560284)
	Portrait.scale:SetStartDelay (0)
	Portrait.scale:SetEndDelay (0)
	Portrait.scale:SetScale (1, 1)
	Portrait.scale:SetOrigin ("center", 0, 0)
    Portrait.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	Portrait.fadeout:SetOrder (2)
	Portrait.fadeout:SetDuration (1)
    Portrait.fadeout:SetStartDelay (4.7000002861023)
	Portrait.fadeout:SetEndDelay (0)
	Portrait.fadeout:SetChange(-1)

	----------------------------------------------

	local DamageIcon  = DetailsNewDamageRecord:CreateTexture ("DamageIconTexture", "OVERLAY")
	DamageIcon:SetTexture ([[Interface\LFGFRAME\UI-LFG-ICON-ROLES]])
	DamageIcon:SetDrawLayer ("OVERLAY", 2)
	DamageIcon:SetPoint ("center", DetailsNewDamageRecord, "center", -97, 1)
	DamageIcon:SetSize (32, 32)
	DamageIcon:SetDesaturated (false)
	DamageIcon:SetTexCoord (0.27200000762939, 0.51899997711182, 0.25837841033936, 0.51399997711182)
	if (0 ~= 0) then
	    DamageIcon:SetRotation (0)
	end
	DamageIcon:SetVertexColor (0.99999779462814, 0.99999779462814, 0.99999779462814, 0.99999779462814)
	DamageIcon:SetAlpha (1)
	DamageIcon:SetBlendMode ("BLEND")

	--> animations for DamageIcon

    DamageIcon.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	DamageIcon.fadein:SetOrder (1)
	DamageIcon.fadein:SetDuration (0.51599997282028)
    DamageIcon.fadein:SetStartDelay (0)
	DamageIcon.fadein:SetEndDelay (0)
	DamageIcon.fadein:SetChange(1)
	DamageIcon.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	DamageIcon.fadeout:SetOrder (2)
	DamageIcon.fadeout:SetDuration (1)
    DamageIcon.fadeout:SetStartDelay (4.5999999046326)
	DamageIcon.fadeout:SetEndDelay (0)
	DamageIcon.fadeout:SetChange(-1)

	----------------------------------------------

	local NewDamageRecord  = DetailsNewDamageRecord:CreateFontString ("NewDamageRecordFontString", "OVERLAY")
	NewDamageRecord:SetFont ([=[Fonts\FRIZQT__.TTF]=], 12, "OUTLINE")
	NewDamageRecord:SetText ("Damage Record!")
	NewDamageRecord:SetDrawLayer ("OVERLAY", 0)
	NewDamageRecord:SetPoint ("center", DetailsNewDamageRecord, "center", 18, 7)
	NewDamageRecord:SetSize (181, 20)
	NewDamageRecord:SetTextColor (1, 1, 1)
	NewDamageRecord:SetAlpha (1)
	NewDamageRecord:SetJustifyH ("CENTER")

	--> animations for NewDamageRecord
    
    NewDamageRecord.standby = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	NewDamageRecord.standby:SetOrder (1)
	NewDamageRecord.standby:SetDuration (0.016000000759959)
    NewDamageRecord.standby:SetStartDelay (0)
	NewDamageRecord.standby:SetEndDelay (0)
	NewDamageRecord.standby:SetChange(0)
    NewDamageRecord.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	NewDamageRecord.fadein:SetOrder (2)
	NewDamageRecord.fadein:SetDuration (0.51599997282028)
    NewDamageRecord.fadein:SetStartDelay (0.40000000596046)
	NewDamageRecord.fadein:SetEndDelay (4.0999999046326)
	NewDamageRecord.fadein:SetChange(1)
	NewDamageRecord.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	NewDamageRecord.fadeout:SetOrder (3)
	NewDamageRecord.fadeout:SetDuration (1)
    NewDamageRecord.fadeout:SetStartDelay (0.10000000149012)
	NewDamageRecord.fadeout:SetEndDelay (0)
	NewDamageRecord.fadeout:SetChange(-1)

	----------------------------------------------

	local DamageAmount  = DetailsNewDamageRecord:CreateFontString ("DamageAmountFontString", "OVERLAY")
	DamageAmount:SetFont ([=[Fonts\FRIZQT__.TTF]=], 12, "THICKOUTLINE")
	DamageAmount:SetText (_detalhes:comma_value (damage))
	DamageAmount:SetDrawLayer ("OVERLAY", 0)
	DamageAmount:SetPoint ("center", DetailsNewDamageRecord, "center", 18, -7)
	DamageAmount:SetSize (100, 20)
	DamageAmount:SetTextColor (1, 1, 1)
	DamageAmount:SetAlpha (1)
	DamageAmount:SetJustifyH ("CENTER")

	--> animations for DamageAmount

    DamageAmount.standby = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	DamageAmount.standby:SetOrder (1)
	DamageAmount.standby:SetDuration (0.016000000759959)
    DamageAmount.standby:SetStartDelay (0)
	DamageAmount.standby:SetEndDelay (0)
	DamageAmount.standby:SetChange(0)
    DamageAmount.fadein = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeIn")
	DamageAmount.fadein:SetOrder (2)
	DamageAmount.fadein:SetDuration (0.51599997282028)
    DamageAmount.fadein:SetStartDelay (0.40000000596046)
	DamageAmount.fadein:SetEndDelay (0)
	DamageAmount.fadein:SetChange(1)
	DamageAmount.fadeout = MainAnimationGroup:CreateAnimation ("ALPHA", "FadeOut")
	DamageAmount.fadeout:SetOrder (3)
	DamageAmount.fadeout:SetDuration (1.0160000324249)
    DamageAmount.fadeout:SetStartDelay (4.2000002861023)
	DamageAmount.fadeout:SetEndDelay (0)
	DamageAmount.fadeout:SetChange(-1)

	--> test the animation
	MainAnimationGroup:Play()


end
